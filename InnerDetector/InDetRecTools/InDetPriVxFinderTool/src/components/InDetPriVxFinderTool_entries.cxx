#include "InDetPriVxFinderTool/InDetPriVxFinderTool.h"
#include "InDetPriVxFinderTool/InDetIterativePriVxFinderTool.h"
#include "InDetPriVxFinderTool/InDetAdaptiveMultiPriVxFinderTool.h"
#include "InDetPriVxFinderTool/InDetMultiPriVxFinderTool.h"
#include "TrkParticleBase/TrackParticleBase.h"


using namespace InDet;

DECLARE_COMPONENT( InDetPriVxFinderTool )
DECLARE_COMPONENT( InDetIterativePriVxFinderTool )
DECLARE_COMPONENT( InDetAdaptiveMultiPriVxFinderTool )
DECLARE_COMPONENT( InDetMultiPriVxFinderTool )

