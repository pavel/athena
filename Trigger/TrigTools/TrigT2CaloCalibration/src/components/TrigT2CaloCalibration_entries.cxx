#include "TrigT2CaloCalibration/T2JESCalibTool.h"
#include "TrigT2CaloCalibration/T2GSCalibTool.h"
#include "TrigT2CaloCalibration/T2SampCalibTool.h"
#include "TrigT2CaloCalibration/EgammaHitsCalibration.h"
#include "TrigT2CaloCalibration/EgammaLWCalibration.h"
#include "TrigT2CaloCalibration/EgammaGapCalibration.h"
#include "TrigT2CaloCalibration/EgammaSshapeCalibration.h"
#include "TrigT2CaloCalibration/EgammaTransitionRegions.h"

DECLARE_COMPONENT( T2JESCalibTool )
DECLARE_COMPONENT( T2GSCalibTool )
DECLARE_COMPONENT( T2SampCalibTool )
DECLARE_COMPONENT( EgammaHitsCalibration )
DECLARE_COMPONENT( EgammaLWCalibration )
DECLARE_COMPONENT( EgammaGapCalibration )
DECLARE_COMPONENT( EgammaSshapeCalibration )
DECLARE_COMPONENT( EgammaTransitionRegions )

